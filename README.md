# ics-ans-role-lab-ioc-common

Ansible role to install common components required to run an IOC in the lab.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-lab-ioc-common
```

## License

BSD 2-clause
